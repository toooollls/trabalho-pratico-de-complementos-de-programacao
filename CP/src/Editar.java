import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import net.proteanit.sql.DbUtils;

import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Editar extends JFrame {

	private JPanel contentPane;
	private JTextField textNome;
	private JTextField textDoses;
	private JTextField textTempoP;
	private JTextField textTempoC;
	private JTextField textTipo;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Editar frame = new Editar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Editar() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 537, 352);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Editar Receita");
		lblNewLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 20));
		lblNewLabel.setForeground(new Color(0, 255, 255));
		lblNewLabel.setBackground(new Color(255, 255, 255));
		lblNewLabel.setBounds(23, 11, 141, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nome");
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setBounds(23, 53, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblDoses = new JLabel("Doses");
		lblDoses.setForeground(new Color(255, 255, 255));
		lblDoses.setBounds(23, 78, 46, 14);
		contentPane.add(lblDoses);
		
		JLabel lblTempoDePreparao = new JLabel("Tempo de Prepara\u00E7\u00E3o");
		lblTempoDePreparao.setForeground(new Color(255, 255, 255));
		lblTempoDePreparao.setBounds(23, 103, 141, 14);
		contentPane.add(lblTempoDePreparao);
		
		JLabel lblTempoDeConfeo = new JLabel("Tempo de Confe\u00E7\u00E3o");
		lblTempoDeConfeo.setForeground(new Color(255, 255, 255));
		lblTempoDeConfeo.setBounds(23, 128, 121, 14);
		contentPane.add(lblTempoDeConfeo);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setForeground(new Color(255, 255, 255));
		lblTipo.setBounds(23, 153, 121, 14);
		contentPane.add(lblTipo);
		
		textNome = new JTextField();
		textNome.setBounds(191, 50, 174, 20);
		contentPane.add(textNome);
		textNome.setColumns(10);
		
		textDoses = new JTextField();
		textDoses.setColumns(10);
		textDoses.setBounds(191, 75, 174, 20);
		contentPane.add(textDoses);
		
		textTempoP = new JTextField();
		textTempoP.setColumns(10);
		textTempoP.setBounds(191, 100, 174, 20);
		contentPane.add(textTempoP);
		
		textTempoC = new JTextField();
		textTempoC.setColumns(10);
		textTempoC.setBounds(191, 125, 174, 20);
		contentPane.add(textTempoC);
		
		textTipo = new JTextField();
		textTipo.setColumns(10);
		textTipo.setBounds(191, 150, 174, 20);
		contentPane.add(textTipo);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 206, 501, 96);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int i = table.getSelectedRow();
				textNome.setText(table.getValueAt(i, 1).toString());
				textDoses.setText(table.getValueAt(i, 2).toString());
				textTempoP.setText(table.getValueAt(i, 3).toString());
				textTempoC.setText(table.getValueAt(i, 4).toString());
				textTipo.setText(table.getValueAt(i, 5).toString());
			}
		});
		scrollPane.setViewportView(table);
		
		JButton btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setForeground(new Color(0, 255, 0));
		btnConfirmar.setBackground(new Color(0, 0, 0));
		btnConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = table.getSelectedRow();
				if (i >= 0) {
					table.setValueAt(textNome.getText(),i, 1);
					table.setValueAt(textDoses.getText(),i, 2);
					table.setValueAt(textTempoP.getText(),i, 3);
					table.setValueAt(textTempoC.getText(),i, 4);
					table.setValueAt(textTipo.getText(),i, 5);
					
				}
				
				try {
					Class.forName("com.mysql.jdbc.Driver");
					java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
					java.sql.Statement stmt = con.createStatement();
					String sql = "UPDATE receita SET  Nome='"+textNome+"', doses='"+textDoses+"', tempoPreparacap='"+textTempoP+"', "
							+ " tempoConfecao='"+textTempoC+"', tipo='"+textTipo+"';";
					stmt.executeUpdate(sql);
					java.sql.ResultSet rs=stmt.executeQuery("SELECT * FROM receita");
					table.setModel(DbUtils.resultSetToTableModel(rs));
					
				}catch(Exception e1) {System.out.print(e1);}
			}
		});
		btnConfirmar.setBounds(375, 49, 136, 23);
		contentPane.add(btnConfirmar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setForeground(new Color(255, 0, 0));
		btnCancelar.setBackground(new Color(0, 0, 0));
		btnCancelar.setBounds(375, 94, 136, 23);
		contentPane.add(btnCancelar);
		
		JButton btnNewButton = new JButton("Receitas Edit\u00E1veis");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
					Statement stmt = con.createStatement();
					String sql = "Select * from receita;";
					ResultSet rs=stmt.executeQuery(sql);
					table.setModel(DbUtils.resultSetToTableModel(rs));
				}catch(Exception e1) {System.out.print(e1);}
				
			}
		});
		btnNewButton.setForeground(new Color(0, 255, 255));
		btnNewButton.setBackground(new Color(0, 0, 0));
		btnNewButton.setBounds(191, 10, 174, 23);
		contentPane.add(btnNewButton);
	}
}
