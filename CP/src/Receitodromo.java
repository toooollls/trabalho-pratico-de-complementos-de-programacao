/**
 * @author Bruno Vieira Augusto, Tom�s Correia Mendes
 * @version 1.3.6
 * @since 24/06/2018
 */
import java.awt.BorderLayout;
import java.awt.*;
import java.awt.EventQueue;
import java.sql.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import net.proteanit.sql.DbUtils;

import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JList;
import javax.swing.JOptionPane;
import java.util.Scanner;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JScrollPane;
import java.util.Scanner;
import javax.swing.JToolBar;
import javax.swing.JTextArea;
import javax.swing.JSlider;
public class Receitodromo extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTable table_ingredientes;
	private JTable tableEtapas;
	private JButton btnNova;
	private JScrollPane scrollPane;
	private JButton btnReceitas;
	private JButton btnIngredientesDisponiveis;
	private JButton btnEtapas;
	private JScrollPane scrollPane_1;
	private JScrollPane scrollPane_2;
	private JButton btnEditar;

	/**
	 * Launch the application.
	 */
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Receitodromo frame = new Receitodromo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Usado para remover as linhas da tabela que s�o seleccionadas
	 * @param table
	 */
	void removeSelectedRows(JTable table){
		   DefaultTableModel model = (DefaultTableModel) this.table.getModel();
		   int i = table.getSelectedRow();
		   if (i > 0) {
			   	model.removeRow(i);
		   }
		   
		   int row = table.getSelectedRow();
		   Object IdReceita = table.getValueAt(row , 0);
		   
		   
		   
		   
		   try {
				Class.forName("com.mysql.jdbc.Driver");
				Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
				Statement stmt = con.createStatement();
				
				String sql = "DELETE FROM receita WHERE idReceita='"+IdReceita+"' ;";
				stmt.executeUpdate(sql);
				ResultSet rs=stmt.executeQuery("DELETE FROM receita WHERE idReceita='"+IdReceita+"' ;");
				table_ingredientes.setModel(DbUtils.resultSetToTableModel(rs));
			}catch(Exception e1) {System.out.print(e1);}
 
		     
		}
	/**
	 * Usado para realizar a ligacao a base de dados
	 * @param void
	 */
	void connectionDatabase() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
			Statement stmt = con.createStatement();
			/*String sql = "Select * from ingredientes";
			ResultSet rs=stmt.executeQuery(sql);
			table_ingredientes.setModel(DbUtils.resultSetToTableModel(rs));
			*/
			
		}catch(Exception e1) {System.out.print(e1);}
	}
	
	/**
	 * Create the frame.
	 */
	public Receitodromo() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Receitodromo.class.getResource("/com/sun/java/swing/plaf/motif/icons/DesktopIcon.gif")));
		setTitle("RECEITODROMO\r\n");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 400, 850, 599);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setForeground(new Color(0, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Receitodromo");
		lblNewLabel.setBounds(337, 6, 160, 24);
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setFont(new Font("Britannic Bold", Font.BOLD | Font.ITALIC, 24));
		contentPane.add(lblNewLabel);
		
		/**
		 * Botao mostrar receitas
		 */
		JButton btnNewButton = new JButton("Mostrar Receitas");
		btnNewButton.setBounds(5, 35, 144, 23);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
					Statement stmt = con.createStatement();
					String sql = "Select * from receita";
					ResultSet rs=stmt.executeQuery(sql);
					table.setModel(DbUtils.resultSetToTableModel(rs));
					
				}catch(Exception e1) {System.out.print(e1);}
			
			}
			
		});
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		btnNewButton.setBackground(Color.BLACK);
		btnNewButton.setForeground(Color.GREEN);
		contentPane.add(btnNewButton);
		
		/**
		 * Introduzir uma nova receita
		 */
		btnNova = new JButton("Nova");
		btnNova.setBounds(178, 35, 76, 23);
		btnNova.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/*String novoNome = JOptionPane.showInputDialog("Nome da Receita");
				String novoDoses = JOptionPane.showInputDialog("Insira o numero de Doses");
				String novoTempoPreparacao = JOptionPane.showInputDialog("Insira o tempo de Preparacao");
				String novoTempoConfecao = JOptionPane.showInputDialog("Insira o tempo de Confecao");
				String novoDescricao = JOptionPane.showInputDialog("Insira a Descricao");
				String novoTipo = JOptionPane.showInputDialog("Insira o Tipo de Receita");
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
					Statement stmt = con.createStatement();
					String sql = "INSERT INTO receita Values ( DEFAULT,'"+novoNome+"', "+novoDoses+","
							+ " "+novoTempoPreparacao+", "+novoTempoConfecao+", '"+novoTipo+"')";
					stmt.executeUpdate(sql);
					ResultSet rs=stmt.executeQuery("SELECT * FROM receita");
					table.setModel(DbUtils.resultSetToTableModel(rs));
				}catch(Exception e1) {System.out.print(e1);} */
				
				Nova nova = new Nova();
				boolean f = true;
				nova.setVisible(f);
				
			}
		});
		btnNova.setForeground(Color.CYAN);
		btnNova.setBackground(Color.BLACK);
		contentPane.add(btnNova);
		
		/**
		 * Apagar receita escolhida
		 */
		JButton apagarReceita = new JButton("Apagar");
		apagarReceita.setBounds(264, 35, 106, 23);
		apagarReceita.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeSelectedRows(table);
				
				

				
			}
		});
		apagarReceita.setBackground(Color.BLACK);
		apagarReceita.setForeground(new Color(255, 0, 0));
		contentPane.add(apagarReceita);
		
		btnReceitas = new JButton("Receitas");
		btnReceitas.setBounds(380, 35, 113, 23);
		btnReceitas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Receitas receitas = new Receitas();
				boolean a = true;
				receitas.setVisible(a);
			}
		});
		btnReceitas.setForeground(Color.MAGENTA);
		btnReceitas.setBackground(Color.BLACK);
		contentPane.add(btnReceitas);
		
		/**
		 * Mostrar ingredientes disponiveis noutra janela
		 */
		btnIngredientesDisponiveis = new JButton("Ingredientes Disponiveis");
		btnIngredientesDisponiveis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Ingredientes ingredientes = new Ingredientes();
				boolean c = true;
				ingredientes.setVisible(c);
			}
		});
		btnIngredientesDisponiveis.setBounds(503, 35, 218, 23);
		btnIngredientesDisponiveis.setForeground(new Color(255, 255, 0));
		btnIngredientesDisponiveis.setBackground(Color.BLACK);
		contentPane.add(btnIngredientesDisponiveis);
		
		btnEtapas = new JButton("Etapas");
		btnEtapas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Etapas etapas = new Etapas();
				boolean d = true;
				etapas.setVisible(d);
			}
		});
		btnEtapas.setBounds(731, 35, 98, 23);
		btnEtapas.setForeground(new Color(153, 50, 204));
		btnEtapas.setBackground(new Color(0, 0, 0));
		contentPane.add(btnEtapas);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(5, 63, 824, 151);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setBackground(Color.WHITE);
		table.setRowHeight(15);
		

		/**
		 * JTable 'table' usada para mostrar as receitas
		 */
		table.addMouseListener(new java.awt.event.MouseAdapter() {
		    @Override
		    public void mouseClicked(java.awt.event.MouseEvent evt) {
		        int row = table.rowAtPoint(evt.getPoint());
		        int col = table.columnAtPoint(evt.getPoint());
		        if (row == 0  && col == 0 ) {
		        	try {
						Class.forName("com.mysql.jdbc.Driver");
						Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
						Statement stmt = con.createStatement();
						String sql = "Select * from ingredientes WHERE nome='Salm�o' OR nome='Aspargo' OR nome='Batata'";
						ResultSet rs=stmt.executeQuery(sql);
						table_ingredientes.setModel(DbUtils.resultSetToTableModel(rs));
					}catch(Exception e1) {System.out.print(e1);} 
		        	try {
						Class.forName("com.mysql.jdbc.Driver");
						Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
						Statement stmt = con.createStatement();
						String sql = "Select * from etapas WHERE idEtapas=1";
						ResultSet rs=stmt.executeQuery(sql);
						tableEtapas.setModel(DbUtils.resultSetToTableModel(rs));
					}catch(Exception e1) {System.out.print(e1);} 
		        	
		        	
		        }	
		    }
		});
		
		table.addMouseListener(new java.awt.event.MouseAdapter() {
		    @Override
		    public void mouseClicked(java.awt.event.MouseEvent evt) {
		        int row = table.rowAtPoint(evt.getPoint());
		        int col = table.columnAtPoint(evt.getPoint());
		        if (row == 1 && col == 0 ){
		        	try {
						Class.forName("com.mysql.jdbc.Driver");
						Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
						Statement stmt = con.createStatement();
						String sql = "Select * from ingredientes where nome='Atum' OR nome='Massa'";
						ResultSet rs=stmt.executeQuery(sql);
						table_ingredientes.setModel(DbUtils.resultSetToTableModel(rs));
					}catch(Exception e1) {System.out.print(e1);} 
		        	try {
						Class.forName("com.mysql.jdbc.Driver");
						Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
						Statement stmt = con.createStatement();
						String sql = "Select * from etapas WHERE idEtapas=2";
						ResultSet rs=stmt.executeQuery(sql);
						tableEtapas.setModel(DbUtils.resultSetToTableModel(rs));
					}catch(Exception e1) {System.out.print(e1);} 
		        }
		    }
		});
		table.addMouseListener(new java.awt.event.MouseAdapter() {
		    @Override
		    public void mouseClicked(java.awt.event.MouseEvent evt) {
		        int row = table.rowAtPoint(evt.getPoint());
		        int col = table.columnAtPoint(evt.getPoint());
		        if (row == 2 && col == 0 ){
		        	try {
						Class.forName("com.mysql.jdbc.Driver");
						Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
						Statement stmt = con.createStatement();
						String sql = "Select * from ingredientes where nome='Banana' OR nome='Leite'";
						ResultSet rs=stmt.executeQuery(sql);
						table_ingredientes.setModel(DbUtils.resultSetToTableModel(rs));
					}catch(Exception e1) {System.out.print(e1);} 
		        	try {
						Class.forName("com.mysql.jdbc.Driver");
						Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
						Statement stmt = con.createStatement();
						String sql = "Select * from etapas WHERE idEtapas=3";
						ResultSet rs=stmt.executeQuery(sql);
						tableEtapas.setModel(DbUtils.resultSetToTableModel(rs));
					}catch(Exception e1) {System.out.print(e1);} 
		        }
		    }
		});
		
		table.addMouseListener(new java.awt.event.MouseAdapter() {
		    @Override
		    public void mouseClicked(java.awt.event.MouseEvent evt) {
		        int row = table.rowAtPoint(evt.getPoint());
		        int col = table.columnAtPoint(evt.getPoint());
		        if (row == 3 && col == 0 ) {
		        	try {
						Class.forName("com.mysql.jdbc.Driver");
						Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
						Statement stmt = con.createStatement();
						String sql = "Select * from ingredientes where nome='Arroz' OR nome='Cogumelos' OR nome='Alho' OR"
								+ " nome='Manteiga' OR nome='Queijo' OR nome='Azeite' OR nome='Cebola'";
						ResultSet rs=stmt.executeQuery(sql);
						table_ingredientes.setModel(DbUtils.resultSetToTableModel(rs));
					}catch(Exception e1) {System.out.print(e1);} 
		        	try {
						Class.forName("com.mysql.jdbc.Driver");
						Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
						Statement stmt = con.createStatement();
						String sql = "Select * from etapas WHERE idEtapas=4";
						ResultSet rs=stmt.executeQuery(sql);
						tableEtapas.setModel(DbUtils.resultSetToTableModel(rs));
					}catch(Exception e1) {System.out.print(e1);} 
		        }
		    }
		});
		
		table.addMouseListener(new java.awt.event.MouseAdapter() {
		    @Override
		    public void mouseClicked(java.awt.event.MouseEvent evt) {
		        int row = table.rowAtPoint(evt.getPoint());
		        int col = table.columnAtPoint(evt.getPoint());
		        if (row == 4 && col == 0 ) {
		        	try {
						Class.forName("com.mysql.jdbc.Driver");
						Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
						Statement stmt = con.createStatement();
						String sql = "Select * from ingredientes where nome='Bifes da Vazia' OR nome='Cebola' OR nome='Alho' OR"
								+ " nome='Manteiga' OR nome='Agua' OR nome='Azeite'";
						ResultSet rs=stmt.executeQuery(sql);
						table_ingredientes.setModel(DbUtils.resultSetToTableModel(rs));
					}catch(Exception e1) {System.out.print(e1);} 
		        	try {
						Class.forName("com.mysql.jdbc.Driver");
						Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
						Statement stmt = con.createStatement();
						String sql = "Select * from etapas WHERE idEtapas=5";
						ResultSet rs=stmt.executeQuery(sql);
						tableEtapas.setModel(DbUtils.resultSetToTableModel(rs));
					}catch(Exception e1) {System.out.print(e1);} 
		        }
		    }
		});
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(5, 259, 639, 294);
		contentPane.add(scrollPane_1);
		
		table_ingredientes = new JTable();
		scrollPane_1.setViewportView(table_ingredientes);
		
		scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(649, 225, 180, 328);
		contentPane.add(scrollPane_2);
		
		tableEtapas = new JTable();
		scrollPane_2.setViewportView(tableEtapas);
		
		JButton btnEntradas = new JButton("Entradas");
		btnEntradas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
					Statement stmt = con.createStatement();
					String sql = "Select * from receita WHERE tipo='Entrada'";
					ResultSet rs=stmt.executeQuery(sql);
					table.setModel(DbUtils.resultSetToTableModel(rs));
				}catch(Exception e1) {System.out.print(e1);} 
			}
		});
		btnEntradas.setForeground(new Color(205, 133, 63));
		btnEntradas.setBackground(Color.BLACK);
		btnEntradas.setBounds(5, 225, 89, 23);
		contentPane.add(btnEntradas);
		
		JButton btnNewButton_2 = new JButton("Prato Principal");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
					Statement stmt = con.createStatement();
					String sql = "Select * from receita WHERE tipo='Prato Principal'";
					ResultSet rs=stmt.executeQuery(sql);
					table.setModel(DbUtils.resultSetToTableModel(rs));
				}catch(Exception e1) {System.out.print(e1);} 
				
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
					Statement stmt = con.createStatement();
					String sql = "Select * from ingredientes'";
					ResultSet rs=stmt.executeQuery(sql);
					table.setModel(DbUtils.resultSetToTableModel(rs));
				}catch(Exception e1) {System.out.print(e1);}
			}
		});
		btnNewButton_2.setForeground(new Color(135, 206, 250));
		btnNewButton_2.setBackground(new Color(0, 0, 0));
		btnNewButton_2.setBounds(104, 225, 119, 23);
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Sobremesa");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
					Statement stmt = con.createStatement();
					String sql = "Select * from receita WHERE tipo='Sobremesa'";
					ResultSet rs=stmt.executeQuery(sql);
					table.setModel(DbUtils.resultSetToTableModel(rs));
				}catch(Exception e1) {System.out.print(e1);} 
			}
		});
		btnNewButton_3.setForeground(new Color(173, 255, 47));
		btnNewButton_3.setBackground(new Color(0, 0, 0));
		btnNewButton_3.setBounds(233, 225, 106, 23);
		contentPane.add(btnNewButton_3);
		
		JLabel calorias = new JLabel("Calorias");
		calorias.setFont(new Font("Tahoma", Font.PLAIN, 16));
		calorias.setForeground(new Color(127, 255, 0));
		calorias.setBackground(new Color(0, 0, 0));
		calorias.setBounds(466, 225, 60, 23);
		contentPane.add(calorias);
		
		btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Editar editar = new Editar();
				boolean g = true;
				editar.setVisible(g);
			}
		});
		btnEditar.setForeground(new Color(255, 255, 255));
		btnEditar.setBackground(Color.BLACK);
		btnEditar.setBounds(351, 225, 89, 23);
		contentPane.add(btnEditar);
		
		JLabel labelCalorias = new JLabel("...........");
		labelCalorias.setFont(new Font("Arial Rounded MT Bold", Font.PLAIN, 16));
		labelCalorias.setForeground(new Color(255, 0, 0));
		labelCalorias.setBackground(new Color(0, 0, 0));
		labelCalorias.setBounds(536, 225, 82, 23);
		contentPane.add(labelCalorias);
		tableEtapas.setRowHeight(3, 30);
	}
}
