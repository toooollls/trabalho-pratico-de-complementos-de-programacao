import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;
import com.mysql.jdbc.Statement;

import net.proteanit.sql.DbUtils;

import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import java.awt.event.ActionListener;
import java.sql.DriverManager;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import java.awt.Insets;

public class Receitas extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JScrollPane scrollPane;
	private JButton sair;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Receitas frame = new Receitas();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Receitas() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JButton mostrarReceitas = new JButton("Mostrar Receitas");
		mostrarReceitas.setForeground(Color.GREEN);
		mostrarReceitas.setBackground(Color.BLACK);
		mostrarReceitas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
					Statement stmt = (Statement) con.createStatement();
					String sql = "Select * from receita";
					ResultSet rs=(ResultSet) stmt.executeQuery(sql);
					table.setModel(DbUtils.resultSetToTableModel(rs));
				}catch(Exception e1) {System.out.print(e1);}
			}
		});
		GridBagConstraints gbc_sair = new GridBagConstraints();
		gbc_sair.insets = new Insets(0, 0, 5, 5);
		gbc_sair.gridx = 0;
		gbc_sair.gridy = 0;
		contentPane.add(mostrarReceitas, gbc_sair);
		
		sair = new JButton("Exit");
		sair.setForeground(new Color(255, 0, 0));
		sair.setBackground(new Color(0, 0, 0));
		sair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		GridBagConstraints gbc_sair1 = new GridBagConstraints();
		gbc_sair1.insets = new Insets(0, 0, 5, 5);
		gbc_sair1.gridx = 1;
		gbc_sair1.gridy = 0;
		contentPane.add(sair, gbc_sair1);
		
		scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridwidth = 10;
		gbc_scrollPane.gridheight = 3;
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 1;
		contentPane.add(scrollPane, gbc_scrollPane);
		table = new JTable();
		scrollPane.setViewportView(table);
	}

}
