import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;
import com.mysql.jdbc.Statement;

import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.DriverManager;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.sql.*;
public class Nova extends JFrame {

	private JPanel contentPane;
	private JTextField textNome;
	private JTextField textDoses;
	private JTextField textTempoP;
	private JTextField textTempoC;
	private JTextField textTipo;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Nova frame = new Nova();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Nova() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 509, 415);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNovaReceita = new JLabel("Nova Receita");
		lblNovaReceita.setFont(new Font("MS UI Gothic", Font.BOLD | Font.ITALIC, 24));
		lblNovaReceita.setForeground(Color.GREEN);
		lblNovaReceita.setBackground(Color.BLACK);
		lblNovaReceita.setBounds(133, 11, 180, 43);
		contentPane.add(lblNovaReceita);
		
		textNome = new JTextField();
		textNome.setBounds(165, 65, 247, 20);
		contentPane.add(textNome);
		textNome.setColumns(10);
		
		textDoses = new JTextField();
		textDoses.setBounds(165, 96, 247, 20);
		contentPane.add(textDoses);
		textDoses.setColumns(10);
		
		textTempoP = new JTextField();
		textTempoP.setColumns(10);
		textTempoP.setBounds(165, 127, 247, 20);
		contentPane.add(textTempoP);
		
		textTempoC = new JTextField();
		textTempoC.setColumns(10);
		textTempoC.setBounds(165, 158, 247, 20);
		contentPane.add(textTempoC);
		
		textTipo = new JTextField();
		textTipo.setColumns(10);
		textTipo.setBounds(165, 189, 247, 20);
		contentPane.add(textTipo);
		
		JLabel lblNewLabel = new JLabel("Nome");
		lblNewLabel.setForeground(Color.CYAN);
		lblNewLabel.setBackground(Color.BLACK);
		lblNewLabel.setBounds(28, 68, 94, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNdoses = new JLabel("n\u00BADoses");
		lblNdoses.setForeground(Color.CYAN);
		lblNdoses.setBackground(Color.BLACK);
		lblNdoses.setBounds(28, 99, 94, 14);
		contentPane.add(lblNdoses);
		
		JLabel lblTempopreparao = new JLabel("tempoPrepara\u00E7\u00E3o");
		lblTempopreparao.setForeground(Color.CYAN);
		lblTempopreparao.setBackground(Color.BLACK);
		lblTempopreparao.setBounds(28, 130, 127, 14);
		contentPane.add(lblTempopreparao);
		
		JLabel lblTempoconfeo = new JLabel("tempoConfe\u00E7\u00E3o");
		lblTempoconfeo.setForeground(Color.CYAN);
		lblTempoconfeo.setBackground(Color.BLACK);
		lblTempoconfeo.setBounds(28, 161, 94, 14);
		contentPane.add(lblTempoconfeo);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setForeground(Color.CYAN);
		lblTipo.setBackground(Color.BLACK);
		lblTipo.setBounds(28, 192, 94, 14);
		contentPane.add(lblTipo);
		
		JButton btnNewButton = new JButton("Confirmar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/*try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
					Statement stmt = (Statement) con.createStatement();
					String sql = "INSERT INTO receita VALUES (DEFAULT, '"+textNome+"', '"+textDoses+"', "+textTempoP+", "+textTempoC+", '"+textTipo+"');";
					stmt.executeUpdate(sql);
					ResultSet rs=(ResultSet) stmt.executeQuery("SELECT * FROM receita;");
					table.setModel(DbUtils.resultSetToTableModel(rs));
				}catch(Exception e1) {System.out.print(e1);} */
				
				String novoNome = textNome.getText();
				String novoDoses = textDoses.getText();
				String novoTempoPreparacao = textTempoP.getText();
				String novoTempoConfecao = textTempoC.getText();
				String novoTipo = textTipo.getText();
				try {
					Class.forName("com.mysql.jdbc.Driver");
					java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
					java.sql.Statement stmt = con.createStatement();
					String sql = "INSERT INTO receita Values (DEFAULT,'"+novoNome+"', '"+novoDoses+"', '"+novoTempoPreparacao+"', '"+novoTempoConfecao+"', '"+novoTipo+"');";
					stmt.executeUpdate(sql);
					java.sql.ResultSet rs=stmt.executeQuery("SELECT * FROM receita;");
					table.setModel(DbUtils.resultSetToTableModel(rs));
				}catch(Exception e1) {System.out.print(e1);}
			}
		});
		btnNewButton.setForeground(Color.BLUE);
		btnNewButton.setBackground(Color.DARK_GRAY);
		btnNewButton.setBounds(323, 220, 104, 23);
		contentPane.add(btnNewButton);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setForeground(Color.RED);
		btnCancelar.setBackground(Color.DARK_GRAY);
		btnCancelar.setBounds(224, 220, 89, 23);
		contentPane.add(btnCancelar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 253, 473, 112);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
	}
}
