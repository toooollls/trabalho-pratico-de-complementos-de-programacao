import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import net.proteanit.sql.DbUtils;

import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.event.ActionEvent;

public class Ingredientes extends JFrame {

	private JPanel contentPane;
	private JTable table;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ingredientes frame = new Ingredientes();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	void removeSelectedRows(JTable table){
		   DefaultTableModel model = (DefaultTableModel)table.getModel();
		   int[] rows = table.getSelectedRows();
		   for(int i=0;i<rows.length;i++){
		     model.removeRow(rows[i]-i);
		   }
		}
	/**
	 * Create the frame.
	 */
	public Ingredientes() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 472, 318);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(128, 128, 128));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnMostrarIngredientes = new JButton("Mostrar Ingredientes");
		btnMostrarIngredientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_cp","root","");
					Statement stmt = con.createStatement();
					String sql = "Select * from ingredientes";
					ResultSet rs=stmt.executeQuery(sql);
					table.setModel(DbUtils.resultSetToTableModel(rs));
					
				}catch(Exception e1) {System.out.print(e1);}
			}
		});
		btnMostrarIngredientes.setForeground(new Color(0, 0, 255));
		btnMostrarIngredientes.setBackground(new Color(0, 0, 0));
		btnMostrarIngredientes.setBounds(10, 11, 133, 23);
		contentPane.add(btnMostrarIngredientes);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 45, 436, 223);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton btnSair = new JButton("Sair");
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				
			}
		});
		btnSair.setForeground(new Color(255, 0, 0));
		btnSair.setBackground(new Color(0, 0, 0));
		btnSair.setBounds(384, 11, 62, 23);
		contentPane.add(btnSair);
		
		JButton btnNovoIngrediente = new JButton("Novo Ingrediente");
		btnNovoIngrediente.setBackground(new Color(0, 0, 0));
		btnNovoIngrediente.setForeground(new Color(0, 255, 127));
		btnNovoIngrediente.setBounds(153, 11, 117, 23);
		contentPane.add(btnNovoIngrediente);
		
		JButton btnNewButton = new JButton("Apagar");
		btnNewButton.setForeground(new Color(255, 99, 71));
		btnNewButton.setBackground(new Color(0, 0, 0));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeSelectedRows(table);
			}
			
		});
		btnNewButton.setBounds(285, 11, 89, 23);
		contentPane.add(btnNewButton);
	}
}
